# jupyter-scipy-devcontainer

Dev ContainerでJupyterLab(`jupyter/scipy-notebook`)を起動するテンプレートプロジェクト。

VSCodeとDev Container機能を使わずに、単純にDockerだけで利用することも可能。

## 使い方

1. このプロジェクトを`git clone`するかzipでダウンロードする。
2. `.git`は削除して、改めて自分のリポジトリとして`git init`する。
3. `README.md`も自分用に書き換える。
4. VSCodeでこのディレクトリを開く。
5. Dev Containerの設定を検出した旨のダイアログが出るので、`Reopen in Container`を選択する。  
   あるいは、コマンドパレットから`Dev Containers: Reopen in Container`を選択する。
6. コマンドパレットから`Create: New Jupyter Notebook`を選択するなどして、新しいノートブックを作成する。
7. 初回実行時にカーネルの選択が表示された場合は、`base (Python 3.x.x) /opt/conda/bin/python`を選択する。

### ワークスペースについて

`./work`ディレクトリがコンテナ内の`/home/jovyan/work`にマウントされており、VSCodeのDev Containerウィンドウではこのディレクトリがワークスペースに設定されている。

### 設定について

Jupyterの設定等は`jupyter-jovyan`ボリュームにマウントされて永続化される。
（実際の名前は`${project_name}_jupyter-jovyan`となる）

#### ボリュームの削除

以下の2通りの方法で削除できる。

1. `docker compose down`時に`--volumes`オプションを指定する
2. `docker volume rm`で削除する

```sh
# down時に--volumesを指定する
docker compose down --volumes
```

```sh
# ボリュームの一覧で対象ボリュームを確認する
docker volume ls

docker volume rm ボリューム名
```

### 終了方法

VSCodeを閉じると自動的にコンテナが停止する（削除はされない）。

## Webアクセス

DockerのログにURLが出力されているので、それを見てアクセスする。

ホスト側（Dev Containerの中ではない）で、`docker compose logs`や`docker logs jupyter-scipy-devcontainer-jupyter-1`等でログを確認する。

以下のようなログが出力されているので、`http://127.0.0.1:8888/lab?token=...`をブラウザで開くとWeb画面にアクセスできる。

```
To access the server, open this file in a browser:
    file:///home/jovyan/.local/share/jupyter/runtime/jpserver-7-open.html
Or copy and paste one of these URLs:
    http://7f4eb0f78116:8888/lab?token=123abcdef
 or http://127.0.0.1:8888/lab?token=123abcdef
```

## Dev Containerを使わずに利用

VSCodeやDev Containerの機能を利用せずに、単純にDockerでJupyterを起動して利用することもできる。

```sh
docker compose up
```

## コンテナ内でsudoしたい場合

環境変数`GRANT_SUDO=1`を設定し、コンテナを`root`ユーザーで起動すると`sudo`が使えるようになる（明示的に`root`で起動しないと`GRANT_SUDO`は効果がないことに注意）。

詳細は公式ドキュメントを参照: https://jupyter-docker-stacks.readthedocs.io/en/latest/using/recipes.html#using-sudo-within-a-container

### sudo有効化の例

```yaml
services:
  jupyter:
    image: jupyter/scipy-notebook:2023-01-16
    ports:
      - 8888:8888
    user: root
    environment:
      TZ: Asia/Tokyo
      GRANT_SUDO: 1
    volumes:
      - jupyter-jovyan:/home/jovyan
      - ./work:/home/jovyan/work

volumes:
  jupyter-jovyan:
```

## 参考：Dev Containerについて

- [Developing inside a Container using Visual Studio Code Remote Development](https://code.visualstudio.com/docs/devcontainers/containers)
- [Development containers](https://containers.dev/)
